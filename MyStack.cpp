// MyStack.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template<typename T>
class Stack
{
public:

    T pop() {
        size--;
        return dinarray[size + 1];

    }

    void push(T a) {
        size++;
        dinarray[size] = a;

    }

    void del() {
        delete[] dinarray;
    }



private:
    int size = 0;
    T *dinarray = new T[10];
};

int main()
{
    Stack<float> stak;
    stak.push(1.1);
    stak.push(2.1);
    stak.push(3.2);
    stak.push(4.2);
    stak.push(2.2);
    std::cout << stak.pop() << '\n';
    std::cout << stak.pop() << '\n';
    std::cout << stak.pop() << '\n';

    stak.del();
}

/*class Stack
{
public:

    int pop() {
        size--;
        return dinarray[size + 1];
        
    }

    void push(int a) {
        size++;
        dinarray[size] = a;

    }

    void del() {
        delete [] dinarray;
    }



private:
    int size = 0;
    int *dinarray = new int[10];
};

int main()
{
    Stack stak;
    stak.push(1);
    stak.push(2);
    stak.push(3);
    stak.push(4);
    stak.push(5);
    std::cout << stak.pop() << '\n';
    std::cout << stak.pop() << '\n';
    std::cout << stak.pop() << '\n';

    stak.del();
}*/

